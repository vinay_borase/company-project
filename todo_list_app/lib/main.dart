import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const ToDoList());
}

class ToDoList extends StatelessWidget {
  const ToDoList({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          title: const Text(
            "To-Do list",
            style: TextStyle(
              fontSize: 25,
            ),
          ),
        ),
        body: ListView.builder(
            padding: const EdgeInsets.only(top: 30, left: 15, right: 15),
            itemCount: 5,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 120,
                width: 330,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: const Color.fromRGBO(250, 232, 232, 1),
                ),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(top: 23, left: 10),
                          child: Container(
                            height: 52,
                            width: 52,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle, color: Colors.white),
                            child: Image.asset("assets/icon.png"),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 14, left: 10),
                          child: Text(
                            "10 June 2023",
                            style: GoogleFonts.quicksand(
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 5, left: 15),
                          height: 20,
                          width: 243,
                          child: Text(
                            "Lorem Ipsum is simply setting industry.",
                            style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w600,
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5, left: 15),
                          height: 50,
                          width: 243,
                          child: Text(
                            "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
                            style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w500,
                              fontSize: 10,
                            ),
                          ),
                        ),
                        const Spacer(),
                        Row(
                          children: [
                            IconButton(
                              padding: const EdgeInsets.only(left: 220),
                              onPressed: () {},
                              icon: const Icon(
                                Icons.edit,
                                size: 15,
                              ),
                            ),
                            IconButton(
                                padding: const EdgeInsets.only(left: 10),
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.delete,
                                  size: 15,
                                )),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              );
            }));
  }
}
