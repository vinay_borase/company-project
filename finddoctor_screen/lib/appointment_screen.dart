import 'package:finddoctor_screen/booking_screen.dart';
import 'package:finddoctor_screen/findDoctor_screen.dart';
import 'package:finddoctor_screen/information.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppointmentScreen extends StatefulWidget {
  const AppointmentScreen(
      {super.key, required this.time, required this.date, required this.index});
  final String time;
  final String date;
  final int index;

  @override
  State<AppointmentScreen> createState() =>
      _AppointmentScreenState(date!, time!, index);
}

class Booking {
  final Image? img;
  final String? name;
  final String? type;
  final String? date1;
  final String? time1;
  final double? price;

  Booking({this.img, this.name, this.type, this.date1, this.time1, this.price});
}

class _AppointmentScreenState extends State<AppointmentScreen> {
  _AppointmentScreenState(this.date, this.time, this.index);

  final String date;
  final String time;
  final int index;

  TextEditingController reasonController = TextEditingController();
  void showBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        isDismissible: true,
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Column(
              children: [
                Text(
                  "Reason",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(25, 154, 142, 1),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextField(
                  controller: reasonController,
                  decoration: InputDecoration(
                    hintText: "Enter reason",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12),
                      borderSide: const BorderSide(
                        color: Color.fromRGBO(25, 154, 142, 1),
                      ),
                    ),
                    border: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(25, 154, 142, 1),
                        ),
                        borderRadius: BorderRadius.circular(12)),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                    onPressed: () {
                      setState(() {});
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Submit",
                      style: GoogleFonts.inter(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  //List<Booking> bookingList = [];

  void submit() {
    setState(() {
      bookingList.add(
        Booking(
          img: doctorList[index].img,
          name: doctorList[index].docName,
          type: doctorList[index].docType,
          date1: date,
          time1: time,
          price: 60,
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back_ios),
          iconSize: 22,
          color: Colors.black,
        ),
        title: Text(
          "Appointment",
          style: GoogleFonts.inter(
            textStyle: const TextStyle(
                fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
          ),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 20, bottom: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(3),
              child: Container(
                margin: const EdgeInsets.only(left: 30),
                height: 120,
                width: 327,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Colors.black45,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Row(
                  children: [
                    Container(
                      height: 88,
                      width: 88,
                      margin: const EdgeInsets.all(10),
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: doctorList[index].img,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            doctorList[index].docName,
                            style: GoogleFonts.inter(
                              textStyle: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black),
                            ),
                          ),
                          Text(
                            doctorList[index].docType,
                            style: GoogleFonts.inter(
                              textStyle: const TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.grey),
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Container(
                            color: Colors.grey,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.star,
                                  color: Color.fromRGBO(25, 154, 142, 1),
                                  size: 18,
                                ),
                                Text(
                                  '${doctorList[index].rating}',
                                  style: const TextStyle(
                                    fontSize: 12,
                                    color: Color.fromRGBO(25, 154, 142, 1),
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: [
                              const Icon(
                                Icons.location_on,
                                size: 18,
                              ),
                              Text(
                                doctorList[index].distance,
                                style: GoogleFonts.inter(
                                  textStyle: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: [
                Text(
                  "Date",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "Change",
                    style: GoogleFonts.inter(
                      textStyle:
                          const TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  height: 36,
                  width: 36,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromRGBO(25, 154, 142, 0.2),
                  ),
                  child: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.calendar_month,
                      color: Color.fromRGBO(25, 154, 142, 1),
                    ),
                  ),
                ),
                Text(
                  '$date$time',
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(85, 85, 85, 1)),
                  ),
                ),
              ],
            ),
            const Text(
              '_____________________________________',
              style: TextStyle(
                fontSize: 20,
                color: Color.fromRGBO(232, 243, 241, 1),
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Row(
              children: [
                Text(
                  "Reason",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "Change",
                    style: GoogleFonts.inter(
                      textStyle:
                          const TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  height: 36,
                  width: 36,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromRGBO(25, 154, 142, 0.2),
                  ),
                  child: IconButton(
                    onPressed: () {
                      showBottomSheet();
                    },
                    icon: const Icon(
                      Icons.note,
                      color: Color.fromRGBO(25, 154, 142, 1),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  reasonController.text,
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                        color: Colors.black),
                  ),
                ),
              ],
            ),
            const Text(
              '_______________________________________',
              style: TextStyle(
                fontSize: 20,
                color: Color.fromRGBO(232, 243, 241, 1),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              "Payment Detail",
              style: GoogleFonts.inter(
                textStyle: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.black),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Text(
                  "Consultation",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(161, 168, 176, 1)),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "60.00",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(16, 22, 35, 1)),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Admin Fee",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(161, 168, 176, 1)),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "01.00",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(16, 22, 35, 1)),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Additional Discount",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(161, 168, 176, 1)),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "-",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(16, 22, 35, 1)),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Total",
                  style: GoogleFonts.inter(
                    textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.black),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "61.00",
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(25, 154, 142, 1)),
                    ),
                  ),
                ),
              ],
            ),
            const Text(
              '________________________________________',
              style: TextStyle(
                fontSize: 20,
                color: Color.fromRGBO(232, 243, 241, 1),
              ),
            ),
            Text(
              "Payment Method",
              style: GoogleFonts.inter(
                textStyle: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.black),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 4, right: 16),
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: Padding(
                    padding: const EdgeInsets.only(top: 5, left: 20),
                    child: Text(
                      "VISA",
                      style: GoogleFonts.inter(
                        textStyle: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(26, 31, 113, 1)),
                      ),
                    ),
                  ),
                  suffix: Padding(
                    padding: const EdgeInsets.only(top: 5, left: 15),
                    child: Text(
                      "Change",
                      style: GoogleFonts.inter(
                        textStyle: const TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(165, 163, 163, 1)),
                      ),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Column(
                  children: [
                    Text(
                      "Total",
                      style: GoogleFonts.inter(
                        textStyle: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: Color.fromRGBO(161, 168, 176, 1)),
                      ),
                    ),
                    Text(
                      "61.00",
                      style: GoogleFonts.inter(
                        textStyle: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w800,
                            color: Color.fromRGBO(16, 22, 35, 1)),
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: SizedBox(
                    height: 50,
                    width: 192,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromRGBO(25, 154, 142, 1),
                      ),
                      onPressed: () {
                        submit();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BookingScreen()));
                      },
                      child: Text(
                        "Booking",
                        style: GoogleFonts.inter(
                          textStyle: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
