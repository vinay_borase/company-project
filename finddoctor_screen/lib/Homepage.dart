import 'package:finddoctor_screen/Adv.dart';
import 'package:finddoctor_screen/card1.dart';
import 'package:finddoctor_screen/docCard.dart';
import 'package:finddoctor_screen/pages/Top_Doctor.dart';
import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  int _index = 0;
  void sel(int index) {
    _index = index;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // Size si = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 24),
            child: Row(
              children: [
                const SizedBox(
                    width: 176,
                    height: 64,
                    child: Text(
                      "Find your desire healt solution",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                    )),
                const Spacer(),
                IconButton(
                    onPressed: () {},
                    icon: const Padding(
                      padding: EdgeInsets.only(right: 5),
                      child: Icon(
                        Icons.notifications_none_sharp,
                      ),
                    ))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Container(
              height: 45,
              width: 324,
              decoration: BoxDecoration(
                  //  border: Border.all(color: Colors.black),
                  color: Color.fromRGBO(232, 243, 241, 1),
                  borderRadius: BorderRadius.circular(20)),
              child: Padding(
                padding: const EdgeInsets.only(left: 10, bottom: 5),
                child: TextFormField(
                  decoration: const InputDecoration(
                      hintText: "Search doctor, drugs, articles...",
                      prefixIcon: Icon(Icons.search_rounded),
                      border: InputBorder.none),
                ),
              ),
            ),
          ),
          //----Doctor---Pharmacy------Hospital---Ambulance--
          card1(),

          //------container--
          avd(),

          //---Doctor Card---
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 1,
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(left: 5),
                      child: Text(
                        "Top Doctor",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: TextButton(
                          onPressed: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return doc_top();
                            }));
                          },
                          child: const Text(
                            "See all",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(25, 154, 142, 1)),
                          )),
                    )
                  ],
                ),
              )
            ],
          ),
          docCard(),
        ]),
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _index,
          onTap: sel,
          type: BottomNavigationBarType.fixed,
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: ""),
            BottomNavigationBarItem(
                icon: Icon(Icons.email_outlined), label: ""),
            BottomNavigationBarItem(
                icon: Icon(Icons.list_alt_outlined), label: ""),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: ""),
          ]),
    );
  }
}
