import 'package:finddoctor_screen/Homepage.dart';
import 'package:finddoctor_screen/appointment_screen.dart';
import 'package:finddoctor_screen/drugsdetail_screen.dart';
import 'package:finddoctor_screen/findDoctor_screen.dart';
import 'package:finddoctor_screen/pages/Doctor_Detail.dart';
import 'package:finddoctor_screen/pharmacyscreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Homepage(),
      debugShowCheckedModeBanner: false,
    );
  }
}