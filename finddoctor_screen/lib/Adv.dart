import 'package:flutter/material.dart';

class avd extends StatelessWidget {
  const avd({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
          height: 135,
          width: 335,
          decoration:
              const BoxDecoration(color: Color.fromRGBO(232, 243, 241, 1)),
          child: Stack(
            children: [
              const Column(
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 10, left: 35),
                        child: SizedBox(
                          height: 50,
                          width: 168,
                          child: Text(
                            "Early protection for your family health",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 225, top: 26),
                child: Container(
                  height: 110,
                  width: 110,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(255, 255, 255, 1)),
                  // color: Colors.amber,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 230),
                child: Image.asset("lib/Icons/7xm 6.png"),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 70, left: 35),
                child: Container(
                  height: 29,
                  width: 97,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color.fromRGBO(25, 154, 142, 1)),
                  child: const Center(
                    child: Text(
                      "Learn more",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(255, 255, 255, 1)),
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
