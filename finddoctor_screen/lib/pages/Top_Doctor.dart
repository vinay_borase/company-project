// ignore: file_names
import 'package:flutter/material.dart';

class doc_top extends StatefulWidget {
  const doc_top({super.key});

  @override
  State<doc_top> createState() => _doc_topState();
}

// ignore: camel_case_types
class _doc_topState extends State<doc_top> {
  List doc = [
    {
      'img': "lib/docImg_top/Image(1).png",
      'name': "Dr. Marcus Horizon",
      'loc': "800m away",
      'type': "Chardiologist",
      'start': "4.7"
    },
    {
      'img': "lib/Icons/Image.png",
      'name': "Dr. Maria Elena",
      'loc': "1,5km away",
      'type': "Psychologist",
      'start': "4.9"
    },
    {
      'img': "lib/docImg_top/Image (3).png",
      'name': "Dr. Stevi Jessi",
      'loc': "2km away",
      'type': 'Orthopedist',
      'start': "4.9"
    },
    {
      'img': "lib/docImg_top/Image (4).png",
      'name': "Dr. Gerty Cori",
      'loc': "800m away",
      'type': 'Orthopedist',
      'start': "4.7"
    },
    {
      'img': "lib/docImg_top/Image (5).png",
      'name': "Dr. Gerty Cori",
      'loc': "800m away",
      'type': 'Orthopedist',
      'start': "4.7"
    },
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40),
                      child: IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(Icons.arrow_back_ios),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 50),
                      child: Text(
                        "Top Doctor",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 100),
                      child: IconButton(
                          onPressed: () {}, icon: const Icon(Icons.menu)),
                    )
                  ],
                ),
              ),
              for (int i = 0; i < doc.length; i++)
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 32, top: 30),
                      child: Container(
                        width: 334,
                        height: 125,
                        decoration: BoxDecoration(
                            border: Border.all(style: BorderStyle.solid),
                            color: const Color.fromRGBO(232, 243, 241, 1)),
                        child: Stack(children: [
                          Container(
                            height: 122,
                            width: 111,
                            child: Image.asset(
                              doc[i]['img'],
                              fit: BoxFit.cover,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 0, top: 0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 140, top: 20),
                                      child: Text(
                                        doc[i]['name'],
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 17),
                                  child: Text(
                                    doc[i]['type'],
                                    style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color:
                                            Color.fromRGBO(173, 173, 173, 1)),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 130, top: 5),
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 5, top: 10),
                                        child: Container(
                                          decoration: const BoxDecoration(
                                              color: Color.fromRGBO(
                                                  232, 243, 241, 1)),
                                          child: Row(
                                            children: [
                                              const Icon(
                                                Icons.star,
                                                size: 14,
                                                color: Color.fromRGBO(
                                                    25, 154, 142, 1),
                                              ),
                                              Text(
                                                doc[i]['start'],
                                                style: const TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                    color: Color.fromRGBO(
                                                        25, 154, 142, 1)),
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(left: 130, top: 2),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.location_on_outlined,
                                        color: Color.fromRGBO(173, 173, 173, 1),
                                      ),
                                      Text(
                                        "800m away",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            color: Color.fromRGBO(
                                                173, 173, 173, 1)),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ]),
                      ),
                    )
                  ],
                )
            ],
          ),
        ),
      ),
    );
  }
}
