import 'package:finddoctor_screen/findDoctor_screen.dart';
import 'package:finddoctor_screen/pharmacyscreen.dart';
import 'package:flutter/material.dart';

class card1 extends StatelessWidget {
  card1({super.key});
  List carddoc = [
    {'img1': "lib/Icons/stethoscope.png", 'name': "Doctor"},
    {'img1': "lib/Icons/drugs.png", 'name': "Pharmacy"},
    {'img1': "lib/Icons/hospital.png", 'name': "Hospital"},
    {'img1': "lib/Icons/ambulance.png", 'name': "Ambulance"}
  ];

  List<Widget> navigatorList = [
    FindDoctorScreen(),
    PharmacyScreen(),
    PharmacyScreen(),
    PharmacyScreen()
  ];

  int i = 0;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 125,
      child: Expanded(
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: carddoc.length,
            itemBuilder: (context, i) {
              return InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => navigatorList[i]));
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 30, top: 25),
                      child: Row(
                        children: [
                          Container(
                            height: 54,
                            width: 54,
                            child: Image.asset(
                              carddoc[i]['img1'],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30, top: 25),
                      child: Text(
                        carddoc[i]['name'],
                        style: const TextStyle(
                            color: Color.fromRGBO(161, 168, 176, 1)),
                      ),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }
}
