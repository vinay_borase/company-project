import 'package:finddoctor_screen/appointment_screen.dart';
import 'package:finddoctor_screen/findDoctor_screen.dart';
import 'package:finddoctor_screen/pharmacyscreen.dart';
import 'package:flutter/material.dart';

List<MedicineInfo> medicineList = [
  MedicineInfo(
      img: const Image(image: AssetImage('assets/medicins/img1.png')),
      medName: 'Panadol',
      contain: '20pcs',
      price: 15.99,
      description:
          'Panadol can be used for relieving fever and/or for the treatment of mild to moderate pain including headache, migraine, muscle ache, dysmenorrhea, sore throat, musculoskeletal pain and pain after dental procedures/ tooth extraction, toothache and pain of osteoarthritis.'),
  MedicineInfo(
      img: const Image(image: AssetImage('assets/medicins/img2.jpg')),
      medName: 'Bodrex Herbal',
      contain: '100ml',
      price: 7.99,
      description:
          'Bodrex has been Indonesians choice of medicine for many generations, to treat headaches, flu, and cough. Since herbal medicine-based traditional medical system of treatment is a rapidly growing healthcare system of economic importance and is now widely used in Indonesia Bodrex introduce a new brand extention line of herbal product .'),
  MedicineInfo(
      img: const Image(image: AssetImage('assets/medicins/img3.png')),
      medName: 'Konidin',
      contain: '3pcs',
      price: 5.99,
      description:
          'Konidin Tablet is used for Cough, Itchy throat/skin, Common cold, Allergy, Hay fever, Watery eyes, Anaphylactic shock, Rhinitis, Urticaria, Pseudobulbar affect and other conditions. Konidin Tablet may also be used for purposes not listed in this medication guide. Konidin Tablet contains Chlorpheniramine Maleate, Dextromethorphan Hbr and Guaifenesin as active ingredients.'),
  MedicineInfo(
      img: const Image(image: AssetImage('assets/medicins/img4.jpg')),
      medName: 'OBH Combi',
      contain: '75ml',
      price: 9.99,
      description:
          'OBH Ika Generic can be used to treat coughs with phlegm. The recommended dosage is 3-4 times daily, one tablespoon for adults, and one teaspoon for children.OBH COMBI  is a cough medicine containing, Paracetamol, Ephedrine HCl, and Chlorphenamine maleate which is used to relieve coughs accompanied by flu symptoms such as fever, headache, and sneezing.'),
  MedicineInfo(
      img: const Image(image: AssetImage('assets/medicins/img5.jpg')),
      medName: 'Betadine',
      contain: '50ml',
      price: 6.99,
      description:
          'Betadine 10% Solution is an antiseptic and disinfectant agent. It is used for the treatment and prevention of infections in wounds and cuts. It kills the harmful microbes and controls their growth, thereby preventing infections in the affected area.'),
  MedicineInfo(
      img: const Image(image: AssetImage('assets/medicins/img6.png')),
      medName: 'Bodrexin',
      contain: '75ml',
      price: 7.99,
      description:
          'Bodrexin is a salicylate. It works by reducing substances in the body that cause pain, fever, and inflammation.Bodrexin is used to treat pain, and reduce fever or inflammation. Bodrexin is sometimes used to treat or prevent heart attacks, strokes, and chest pain (angina). Bodrexin should be used for cardiovascular conditions only under the supervision of a doctor.'),
];

List<DoctorInfo> doctorList = [
  DoctorInfo(
    img: const Image(image: AssetImage('assets/doctor/doctor1.png')), 
    docName: 'Dr. Marcus Horizon', 
    docType: 'Chardiologist', 
    rating: 4.7, 
    distance: '800m away',
    about: 'A cardiologist is a physician whos an expert in the care of your heart and blood vessels. They can treat or help you prevent a number of cardiovascular problems. They can also specialize in specific areas, such as abnormal heart rhythms, heart failure or heart problems you’ve had since birth.'
  ),
  DoctorInfo(
    img: const Image(image: AssetImage('assets/doctor/doctor2.png')), 
    docName: 'Dr. Maria Robert', 
    docType: 'Dentist', 
    rating: 4.2, 
    distance: '1.2km away', 
    about: 'Dentists are trained professionals who help care for the teeth and mouth. Regularly seeing a dentist can help you to maintain a good level of dental health, which may have a direct impact on your overall well-being.'),
  DoctorInfo(
    img: const Image(image: AssetImage('assets/doctor/doctor3.png')), 
    docName: 'Dr. Stevi Jackson', 
    docType: 'Psychiatrist', 
    rating: 4.0, 
    distance: '400m away', 
    about: 'A psychiatrist is a medical doctor who’s an expert in the field of psychiatry — the branch of medicine focused on the diagnosis, treatment and prevention of mental, emotional and behavioral disorders.Psychiatrists assess both the mental and physical aspects of psychological conditions. They can diagnose and treat these conditions.'),
  DoctorInfo(
    img: const Image(image: AssetImage('assets/doctor/doctor4.png')), 
    docName: 'Dr. Luke Abraham', 
    docType: 'Surgeon', 
    rating: 4.8, 
    distance: '1.5km away', 
    about: 'A surgeon is a doctor who specializes in evaluating and treating conditions that may require surgery, or physically changing the human body. Surgeries can be done to diagnose or treat disease or injury. In the operating room, surgeons lead a team of other doctors and nurses to make sure that a procedure goes smoothly.'),
];


  List<Booking> bookingList = [];
