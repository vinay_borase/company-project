import 'package:flutter/material.dart';

class DoctorInfo extends ChangeNotifier {
  List doc = [
    {
      'img': "lib/Icons/doc/aV.png",
      'name': "Dr. Marcus Horizon",
      'loc': "800m away",
      'type': "Chardiologist",
      'start': "4.7"
    },
    {
      'img': "lib/Icons/doc/Avatar1.png",
      'name': "Dr. Maria Elena",
      'loc': "1,5km away",
      'type': "Psychologist",
      'start': "4.9"
    },
    {
      'img': "lib/Icons/doc/Avatar2.png",
      'name': "Dr. Stevi Jessi",
      'loc': "2km away",
      'type': 'Orthopedist',
      'start': "4.9"
    },
  ];
}
