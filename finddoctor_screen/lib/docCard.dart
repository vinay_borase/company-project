import 'package:flutter/material.dart';

class docCard extends StatefulWidget {
  const docCard({super.key});

  @override
  State<docCard> createState() => _docCardState();
}

// ignore: camel_case_types
class _docCardState extends State<docCard> {
  List doc = [
    {
      'img': "lib/Icons/doc/aV.png",
      'name': "Dr. Marcus Horizon",
      'loc': "800m away",
      'type': "Chardiologist",
      'start': "4.7"
    },
    {
      'img': "lib/Icons/doc/Avatar1.png",
      'name': "Dr. Maria Elena",
      'loc': "1,5km away",
      'type': "Psychologist",
      'start': "4.9"
    },
    {
      'img': "lib/Icons/doc/Avatar2.png",
      'name': "Dr. Stevi Jessi",
      'loc': "2km away",
      'type': 'Orthopedist',
      'start': "4.9"
    },
  ];
  int i = 0;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          for (i = 0; i < doc.length; i++)
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                height: 203,
                width: 151,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color.fromRGBO(232, 243, 241, 1)),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 120,
                        width: 120,
                        decoration: const BoxDecoration(shape: BoxShape.circle),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: Center(
                            child: Image.asset(
                              doc[i]['img'],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: SizedBox(
                          child: Text(
                            doc[i]['name'],
                            style: const TextStyle(
                                fontSize: 15, fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(
                          doc[i]['type'],
                          style: const TextStyle(
                              fontSize: 10,
                              color: Color.fromRGBO(173, 173, 173, 1)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, top: 12),
                        child: Row(
                          children: [
                            const Icon(
                              Icons.star,
                              size: 12,
                              color: Color.fromRGBO(25, 154, 142, 1),
                            ),
                            Text(
                              doc[i]['start'],
                              style: const TextStyle(
                                fontSize: 10,
                                color: Color.fromRGBO(25, 154, 142, 1),
                              ),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            const Icon(
                              Icons.location_on,
                              size: 8,
                              color: Color.fromRGBO(173, 173, 173, 1),
                            ),
                            const Text(
                              "800m away",
                              style: TextStyle(
                                  fontSize: 8,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(173, 173, 173, 1)),
                            )
                          ],
                        ),
                      )
                    ]),
              ),
            )
        ],
      ),
    );
  }
}
