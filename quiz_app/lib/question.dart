
import 'package:quiz_app/homescreen.dart';

List javaQuestion = [
  const QuestionModel(
    question: "Number of primitive data types in Java are?",
    options: ["6", "7", "8", "9"],
    answerIndex: 2,
  ),
  const QuestionModel(
    question:
        "Automatic type conversion is possible in which of the possible cases?",
    options: ["Byte to int", "Int to long", "long to int", "Short to int"],
    answerIndex: 1,
  ),
  const QuestionModel(
    question: "Select the valid statement.",
    options: [
      "char ch[]=new char(5)",
      "char ch[]=new char[5]",
      "char ch[]=new char()",
      "char ch[]=new char[]"
    ],
    answerIndex: 1,
  ),
  const QuestionModel(
    question: "Which one of the following is not a Java feature?",
    options: [
      "Object-oriented",
      "Use of pointers",
      "Portable",
      "Dynamic and Extensible"
    ],
    answerIndex: 1,
  ),
  const QuestionModel(
    question: "What is the extension of java code files?",
    options: [".js", ".txt", ".class", ".java"],
    answerIndex: 3,
  ),
];

List dartQuestion = [
  const QuestionModel(
    question: "What is Dart?",
    options: [
      "A programming language",
      "A web development framework",
      " A database management system",
      "A hardware device"
    ],
    answerIndex: 0,
  ),
  const QuestionModel(
    question: "Dart is originally developed by?",
    options: ["Microsoft", "Google", "IBM", "Facebook"],
    answerIndex: 1,
  ),
  const QuestionModel(
    question: "Dart is an Object-Oriented language",
    options: ["Yes", "No", "Can be yes or no", "Can not say"],
    answerIndex: 0,
  ),
  const QuestionModel(
    question: "An ________ is a real-time representation of any entity.",
    options: ["class", "method", "object", "None of the above"],
    answerIndex: 2,
  ),
  const QuestionModel(
    question: "The _______ function is a predefined method in Dart.",
    options: ["declare()", "list()", "main()", "return()"],
    answerIndex: 2,
  ),
];

List pythonQuestion = [
  const QuestionModel(
    question: " Who developed Python Programming Language?",
    options: [
      "Wick van Rossum",
      "Rasmus Lerdorf",
      "Guido van Rossum",
      "Niene Stom"
    ],
    answerIndex: 2,
  ),
  const QuestionModel(
    question:
        "Which of the following is the correct extension of the Python file?",
    options: [".python", ".py", ".pl", ".p"],
    answerIndex: 1,
  ),
  const QuestionModel(
    question: " All keywords in Python are in _________",
    options: [
      "Capitalized",
      "lower case",
      "UPPER CASE",
      "None of the mentioned"
    ],
    answerIndex: 3,
  ),
  const QuestionModel(
    question: "Which keyword is used for function in Python language?",
    options: ["Function", "def", "Fun", "Define"],
    answerIndex: 1,
  ),
  const QuestionModel(
    question: "Which of the following is a Python tuple?",
    options: ["{1, 2, 3}", "{}", "[1, 2, 3]", "(1, 2, 3)"],
    answerIndex: 3,
  ),
];

List cQuestion = [
  const QuestionModel(
    question: "Which of the following is the correct syntax to add the header file in the C++ program?",
    options: ["#include<userdefined>", "#include userdefined.h", "<include> userdefined.h", "Both A and B"],
    answerIndex: 3,
  ),
  const QuestionModel(
    question: "Which of the following is the correct identifier?",
    options: ["var_name", "VAR_123", "varname@", "None of the above"],
    answerIndex: 1,
  ),
  const QuestionModel(
    question: "Which of the following is the address operator?",
    options: ["@", "#", "&", "%"],
    answerIndex: 2,
  ),
  const QuestionModel(
    question: "C++ is a ___ type of language",
    options: ["High-level Language", "Low-level language", "Middle-level language", "None of the above"],
    answerIndex: 2,
  ),
  const QuestionModel(
    question: "Which types of arrays are always considered as linear arrays?",
    options: ["Single-dimensional", "Multi-dimensional", "Both A and B", "None of the above"],
    answerIndex: 0,
  ),
];

 List allQuestions = [
    const QuestionModel(
      question: "Who is the founder of Microsoft ?",
      options: ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      answerIndex: 2,
    ),
    const QuestionModel(
      question: "Who is the founder of Apple ?",
      options: ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      answerIndex: 0,
    ),
    const QuestionModel(
      question: "Who is the founder of Amazon ?",
      options: ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      answerIndex: 1,
    ),
    const QuestionModel(
      question: "Who is the founder of Tesla ?",
      options: ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      answerIndex: 3,
    ),
    const QuestionModel(
      question: "Who is the founder of Google ?",
      options: ["Steve Jobs", "Lary Page", "Bill Gates", "Elon Musk"],
      answerIndex: 1,
    ), 
  ];
