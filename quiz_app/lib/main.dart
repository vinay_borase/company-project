import 'package:flutter/material.dart';
import 'package:quiz_app/titlescreen.dart';

void main() => runApp(const QuizApp());

class QuizApp extends StatelessWidget {
  const QuizApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: TitleScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}


