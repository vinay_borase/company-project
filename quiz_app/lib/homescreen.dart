import 'package:flutter/material.dart';
import 'package:quiz_app/mainscreen.dart';
import 'package:quiz_app/question.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen(this.indexInfo, {super.key});

  final int indexInfo;

  @override
  State createState() => _HomeScreenState(indexInfo);
}

class QuestionModel {
  final String? question;
  final List<String>? options;
  final int? answerIndex;

  const QuestionModel({this.question, this.options, this.answerIndex});
}

class _HomeScreenState extends State {
  _HomeScreenState(this.indexInfo);

  int indexInfo;
  List<List> menuList = [
    dartQuestion,
    javaQuestion,
    pythonQuestion,
    cQuestion,
    allQuestions
  ];

  bool questionScreen = true;
  int questionIndex = 0;
  int selectedAnswerIndex = -1;
  int noOfCorretAnswer = 0;
  String resultText = "Congratulation";
  int flag = 0;

  Color checkedAnswer(int buttonIndex) {
    if (selectedAnswerIndex != -1) {
      if (buttonIndex == menuList[indexInfo][questionIndex].answerIndex) {
        return Colors.green;
      } else if (buttonIndex == selectedAnswerIndex) {
        return Colors.red;
      } else {
        return Colors.white;
      }
    } else {
      return Colors.white;
    }
  }

  Text nextPage() {
    if (selectedAnswerIndex == -1 && flag == 1) {
      return const Text(
        "Please select option!",
        style: TextStyle(
          fontSize: 20,
          color: Colors.red,
        ),
      );
    } else {
      return const Text(" ");
    }
  }

  void nextPageScreen() {
    if (selectedAnswerIndex == -1) {
      setState(() {
        flag = 1;
        nextPage();
      });
      return;
    }

    if (selectedAnswerIndex == menuList[indexInfo][questionIndex].answerIndex) {
      noOfCorretAnswer += 1;
    }
    if (questionIndex == menuList[indexInfo].length - 1) {
      questionScreen = false;
    }
    questionIndex += 1;
    setState(() {
      flag = 0;
      selectedAnswerIndex = -1;
    });
  }

  Image resultImage() {
    if (noOfCorretAnswer >= 3) {
      resultText = "Congratulation";
      return Image.asset("assets/winner.png");
    } else {
      resultText = "Failed";
      return Image.asset("assets/fail.png");
    }
  }

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        backgroundColor: Colors.black87,
        appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.black,
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Questions : ",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                    color: Colors.white60,
                  ),
                ),
                Text(
                  "${questionIndex + 1}/${menuList[indexInfo].length}",
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                    color: Colors.white60,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            SizedBox(
              width: 350,
              //height: 100,
              child: Text(
                menuList[indexInfo][questionIndex].question,
                style: const TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.w400,
                  color: Color.fromARGB(255, 236, 234, 234),
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 300,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black87,
                  side: BorderSide(
                    width: 2,
                    color: checkedAnswer(0),
                  ),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 0;
                    });
                  }
                },
                child: Text(
                  "A.${menuList[indexInfo][questionIndex].options[0]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            SizedBox(
              width: 300,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black87,
                  side: BorderSide(
                    width: 2,
                    color: checkedAnswer(1),
                  ),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 1;
                    });
                  }
                },
                child: Text(
                  "B.${menuList[indexInfo][questionIndex].options[1]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            SizedBox(
              width: 300,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black87,
                  side: BorderSide(
                    width: 2,
                    color: checkedAnswer(2),
                  ),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 2;
                    });
                  }
                },
                child: Text(
                  "C.${menuList[indexInfo][questionIndex].options[2]}",
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            SizedBox(
              width: 300,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black87,
                  side: BorderSide(
                    width: 2,
                    color: checkedAnswer(3),
                  ),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 3;
                    });
                  }
                },
                child: Text(
                  "D.${menuList[indexInfo][questionIndex].options[3]}",
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            SizedBox(
              height: 50,
              child: nextPage(),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: nextPageScreen,
          backgroundColor: Colors.blue,
          child: const Icon(
            Icons.forward,
            color: Colors.orange,
          ),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Colors.black87,
        appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.black,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 400,
                width: 280,
                child: resultImage(),
              ),
              Text(
                resultText,
                style: const TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "You have compeleted the Quiz",
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.normal,
                  color: Colors.white,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                "$noOfCorretAnswer/${menuList[indexInfo].length}",
                style: const TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black87,
                  side: const BorderSide(
                    width: 2,
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  selectedAnswerIndex = -1;
                  noOfCorretAnswer = 0;
                  questionIndex = 0;
                  questionScreen = true;
                  setState(() {});
                },
                child: const Text(
                  "Reset",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.red,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.black87,
                  side: const BorderSide(
                    width: 2,
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const MainScreen()));
                },
                child: const Text(
                  "Menu",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.red,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: isQuestionScreen(),
    );
  }
}
