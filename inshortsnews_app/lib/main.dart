import 'package:flutter/material.dart';
import 'package:inshortsnews_app/login_screen.dart';

void main() {
  runApp(const InshortsApp());
}

class InshortsApp extends StatefulWidget {
  const InshortsApp({super.key});

  @override
  State<InshortsApp> createState() => _InshortsAppState();
}

class _InshortsAppState extends State<InshortsApp> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}