import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inshortsnews_app/Model/model_class.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool allDemo = false;
  bool nationalDemo = true;
  bool businessDemo = false;
  bool sportsDemo = false;
  bool worldDemo = false;
  bool politicsDemo = false;
  bool technologyDemo = false;
  bool startupDemo = false;
  bool entertainmentDemo = false;
  bool miscellaneousDemo = false;
  bool hatkeDemo = false;
  bool scienceDemo = false;
  bool automobileDemo = false;

  Color checkDemo(bool value) {
    if (value == true) {
      return const Color.fromRGBO(33, 33, 33, 0.4);
    } else {
      return Colors.transparent;
    }
  }

  List<DataDemo> demoList = [];

  Future<List<DataDemo>> fetchUsers() async {
    try {
      const url = 'https://inshortsapi.vercel.app/news?category=national';
      final uri = Uri.parse(url);
      final response = await http.get(uri);
      final body = response.body;
      final json = jsonDecode(body);
      final results = json['data'] as List<dynamic>;
      final transformed = results.map((e) {
        return DataDemo(
          author: e['author'],
          content: e['content'],
          date: e['date'],
          id: e['id'],
          imageUrl: e['imageUrl'],
          readMoreUrl: e['readMoreUrl'],
          time: e['time'],
          title: e['title'],
          url: e['url'],
        );
      }).toList();
      demoList = transformed;
    } catch (e) {
      print(e);
    }
    return demoList;
  }

  @override
  Widget build(BuildContext context) {
    double screenHight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'My Feed',
          style: GoogleFonts.poppins(
            textStyle: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(0, 0, 0, 1)),
          ),
        ),
        centerTitle: true,
      ),
      body: FutureBuilder(
          future: fetchUsers(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Text('Loading');
            } else {
              return PageView.builder(
                scrollDirection: Axis.vertical,
                  itemCount: demoList.length,
                  itemBuilder: (context, index) {
                    final users = demoList[index];
                    final titel1 = users.title;
                    return Column(
                      children: [
                        Container(
                          height: screenHight * 0.3,
                          width: screenWidth,
                          child: Image.network(
                            demoList[index].imageUrl.toString(),
                            fit: BoxFit.cover,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            children: [
                              const SizedBox(
                                height: 20,
                              ),
                              Text(
                                titel1.toString(),
                                style: GoogleFonts.poppins(
                                  textStyle: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      color: Color.fromRGBO(0, 0, 0, 1)),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Text(
                                demoList[index].content.toString(),
                                style: GoogleFonts.poppins(
                                  textStyle: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      color: Color.fromRGBO(33, 33, 33, 0.7)),
                                ),
                              ),
                              const SizedBox(
                                height: 13,
                              ),
                              Row(
                                children: [
                                  Text(
                                    demoList[index].author.toString()+' /',
                                    style: GoogleFonts.poppins(
                                      textStyle: const TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              Color.fromRGBO(33, 33, 33, 0.4)),
                                    ),
                                  ),
                                  Text(
                                    demoList[index].date.toString() +
                                        " " +
                                        demoList[index].time.toString(),
                                    style: GoogleFonts.poppins(
                                      textStyle: const TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              Color.fromRGBO(33, 33, 33, 0.4)),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        const Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                height: screenHight * 0.08,
                                width: screenWidth * 0.08,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    width: 1,
                                  ),
                                ),
                                child:
                                    const Icon(Icons.bookmark_border_outlined),
                              ),
                            ),
                            const SizedBox(
                              width: 7,
                            ),
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                height: screenHight * 0.08,
                                width: screenWidth * 0.08,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    width: 1,
                                  ),
                                ),
                                child: const Icon(Icons.share),
                              ),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 3,
                        ),
                        GestureDetector(
                          onTap: () {
                            demoList[index].url;
                          },
                          child: Container(
                            padding: const EdgeInsets.only(left: 15, top: 2),
                            height: screenHight * 0.06,
                            width: screenWidth,
                            color: const Color.fromRGBO(33, 33, 33, 0.5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Tap Here',
                                  style: GoogleFonts.poppins(
                                    textStyle: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 1)),
                                  ),
                                ),
                                Text(
                                  'To Read More',
                                  style: GoogleFonts.poppins(
                                    textStyle: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 1)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  });
            }
          }),
      drawer: Drawer(
          width: screenWidth * 0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  Text(
                    'News App',
                    style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                  ),
                  Text(
                    'Categories',
                    style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(0, 0, 0, 0.8),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      allDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(allDemo),
                      ),
                      child: Center(
                        child: Text(
                          'All',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      nationalDemo = true;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(nationalDemo),
                      ),
                      child: Center(
                        child: Text(
                          'National',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      businessDemo = true;
                                            nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(businessDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Business',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      sportsDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(sportsDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Sports',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      worldDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(worldDemo),
                      ),
                      child: Center(
                        child: Text(
                          'World',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      politicsDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(politicsDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Politics',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      technologyDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(technologyDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Technology',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      startupDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(startupDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Startup',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      entertainmentDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(entertainmentDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Entertainment',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      miscellaneousDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(miscellaneousDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Miscellaneous',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      hatkeDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(hatkeDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Hatke',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      scienceDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(scienceDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Science',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      automobileDemo = true;
                      nationalDemo = false;
                      setState(() {});
                    },
                    child: Container(
                      width: 184,
                      height: 40,
                      padding: const EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20)),
                        color: checkDemo(automobileDemo),
                      ),
                      child: Center(
                        child: Text(
                          'Automobile',
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
