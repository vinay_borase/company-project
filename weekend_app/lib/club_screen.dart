import 'package:flutter/material.dart';
import 'package:weekend_app/fort_info.dart';
import 'package:weekend_app/home1_screen.dart';

class ClubScreen extends StatefulWidget {
  const ClubScreen({super.key});

  @override
  State<ClubScreen> createState() => _ClubScreenState();
}

class _ClubScreenState extends State<ClubScreen> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    String info = "c";

    String textDemo(int index) {
      setState(() {
        info = description[index];
      });
      return info;
    }

    TabController _tabController = TabController(length: 3, vsync: this);
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 31, 30, 30),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Home1Screen(),
                  ));
            },
            icon: Icon(Icons.arrow_back_ios)),
        title: Text("Club"),
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Container(
                          height: 200,
                          width: 330,
                          child: Image.asset(
                            "assets/images/sinhgad/s2.jpg",
                            fit: BoxFit.fill,
                          ),
                          color: Colors.blue,
                          margin: EdgeInsets.only(left: 15, right: 10),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: TabBar(
                      controller: _tabController,
                      labelColor: Colors.white,
                      unselectedLabelColor: Colors.white60,
                      isScrollable: true,
                      tabs: [
                        Tab(
                          text: "Plan",
                        ),
                        Tab(
                          text: "Info",
                        ),
                        Tab(
                          text: "Content",
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            height: 400,
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(15),
              child: Column(
                children: [
                  Container(
                    child: const Text(
                      "Description ",
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    alignment: Alignment.topLeft,
                  ),
                  Text(
                    textDemo(0),
                    style: const TextStyle(fontSize: 17, color: Colors.white),
                  ),
                  Container(
                    height: 250,
                    width: 350,
                    child: Image.asset(
                      "assets/images/sinhgad/s12.jpg",
                      fit: BoxFit.fill,
                    ),
                    color: Colors.blue,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                  ),
                  Container(
                    child: const Text(
                      "Trail Information",
                      style: TextStyle(
                          fontSize: 17,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                    alignment: Alignment.topLeft,
                  ),
                  Text(
                    info2,
                    style: const TextStyle(fontSize: 17, color: Colors.white),
                  ),
                  Container(
                    height: 250,
                    width: 350,
                    child: Image.asset(
                      "assets/images/sinhgad/s15.jpg",
                      fit: BoxFit.fill,
                    ),
                    color: Colors.blue,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                  ),
                  Container(
                    height: 250,
                    width: 350,
                    child: Image.asset(
                      "assets/images/sinhgad/s11.jpg",
                      fit: BoxFit.fill,
                    ),
                    color: Colors.blue,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                  ),
                  Container(
                    height: 250,
                    width: 350,
                    child: Image.asset(
                      "assets/images/sinhgad/s14.jpg",
                      fit: BoxFit.fill,
                    ),
                    color: Colors.blue,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                  ),
                  Container(
                    height: 250,
                    width: 350,
                    child: Image.asset(
                      "assets/images/sinhgad/s13.jpg",
                      fit: BoxFit.fill,
                    ),
                    color: Colors.blue,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomSheet: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            const Icon(Icons.currency_rupee_rounded),
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.indeterminate_check_box_outlined),
            ),
            const Text(
              "120",
              style:
                  TextStyle(decoration: TextDecoration.underline, fontSize: 20),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.add_box_outlined),
            ),
            const Spacer(),
            ElevatedButton(
              onPressed: () {},
              child: const Text(
                "Book Trip Now >>",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
