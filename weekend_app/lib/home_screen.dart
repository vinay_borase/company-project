import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: AppBar(
        title: Image.asset(
          "assets/images/w1.png",
          height: 120,
          width: 150,
        ),
        backgroundColor: Colors.black,
        actions: <Widget>[
          TextButton(
              onPressed: () {},
              child: const Text(
                "login",
                style: TextStyle(
                  fontSize: 17,
                ),
              ))
        ],
      ),
      body: Column(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Container(
                  height: 200,
                  width: 360,
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    image: const DecorationImage(image: AssetImage("assets/images/c1.jpg")),
                    border: Border.all(color: Colors.white),
                    //borderRadius: BorderRadius.circular(15),
                  ),
                  child: Text("Eskobar", style: TextStyle(color: Colors.white)),
                  alignment: Alignment.bottomLeft,
                  //margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/c2.jpg"),
                  height: 200,
                  //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/s1.jpg"),
                  height: 200,
                  //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/wed1.jpg"),
                  height: 200,
                  //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/c3.jpg"),
                  height: 200,
                  //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/wed2.jpg"),
                  height: 200,
                  //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/wed3.jpeg"),
                  height: 200,
                 //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/t1.jpg"),
                  height: 200,
                  //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
                Container(
                  child: Image.asset("assets/images/c4.jpg"),
                  height: 200,
                  //width: 360,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  margin: EdgeInsets.all(25),
                ),
              ],
            ),
          ),
          Container(
            child: const Text("Clubs",style: TextStyle(fontSize: 25,fontStyle: FontStyle.italic, color: Colors.white),),
            alignment: Alignment.topLeft,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Container(
                  height: 160,
                  width: 230,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/c5.jpg"),alignment: Alignment.topLeft,),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.white),
                  ),
                  margin: EdgeInsets.all(10),
                  child: Padding(
                    padding:  EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text("Eskobar",style: TextStyle(fontSize: 17,),),),
                          Spacer(),
                          Text("4.4",style: TextStyle(fontSize: 17),),
                          Icon(Icons.star_rate,size: 19,),
                      ],
                    ),
                  ),
                  alignment: Alignment.bottomLeft,
                ),
                Container(
                  height: 160,
                  width: 230,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/c6.jpg"),alignment: Alignment.topLeft,),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.white),
                  ),
                  margin: EdgeInsets.all(10),
                  child: Padding(
                    padding:  EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text("Eskobar",style: TextStyle(fontSize: 17,),),),
                          Spacer(),
                          Text("4.4",style: TextStyle(fontSize: 17),),
                          Icon(Icons.star_rate,size: 19,),
                      ],
                    ),
                  ),
                  alignment: Alignment.bottomLeft,
                ),
                Container(
                  height: 160,
                  width: 230,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/c1.jpg"),alignment: Alignment.topLeft,),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.white),
                  ),
                  margin: EdgeInsets.all(10),
                  child: Padding(
                    padding:  EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text("Eskobar",style: TextStyle(fontSize: 17,),),),
                          Spacer(),
                          Text("4.4",style: TextStyle(fontSize: 17),),
                          Icon(Icons.star_rate,size: 19,),
                      ],
                    ),
                  ),
                  alignment: Alignment.bottomLeft,
                ),
                Container(
                  height: 160,
                  width: 230,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/c3.jpg"),alignment: Alignment.topLeft),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.white),
                  ),
                  margin: EdgeInsets.all(10),
                  child: Padding(
                    padding:  EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text("Eskobar",style: TextStyle(fontSize: 17,),),),
                          Spacer(),
                          Text("4.4",style: TextStyle(fontSize: 17),),
                          Icon(Icons.star_rate,size: 19,),
                      ],
                    ),
                  ),
                  alignment: Alignment.bottomLeft,
                ),
                Container(
                  height: 160,
                  width: 230,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/images/c4.jpg"),alignment: Alignment.topLeft,),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.white),
                  ),
                  margin: EdgeInsets.all(10),
                  child: Padding(
                    padding:  EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text("Eskobar",style: TextStyle(fontSize: 17,),),),
                          Spacer(),
                          Text("4.4",style: TextStyle(fontSize: 17),),
                          Icon(Icons.star_rate,size: 19,),
                      ],
                    ),
                  ),
                  alignment: Alignment.bottomLeft,
                ),
              ],
            ),
          ),
          Container(
            child: const Text("Trek",style: TextStyle(fontSize: 25,fontStyle: FontStyle.italic, color: Colors.white),),
            alignment: Alignment.topLeft,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 170,
                width: 230,
                decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("assets/images/c3.jpg"),alignment: Alignment.topLeft,fit: BoxFit.fill),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.white),
                ),
                child: Container(
                  alignment: Alignment.bottomLeft,
                  height: 30,
                  width: 230,
                  //color: Colors.blue,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15),bottomRight: Radius.circular(15)),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(4.5),
                    child: Row(
                      children: [
                        Text("Eskobar",style: TextStyle(fontSize: 17,),),
                        Spacer(),
                        Text("4.4",style: TextStyle(fontSize: 17),),
                        Icon(Icons.star_rate,size: 19,),
                      ],
                    ),
                  ),
                ),
                alignment: Alignment.bottomLeft,
              ),
            ],
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
            backgroundColor: Colors.black,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: "Search",
            backgroundColor: Colors.black,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.video_camera_back_outlined),
            label: "Reels",
            backgroundColor: Colors.black,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline_outlined),
            label: "Profile",
            backgroundColor: Colors.black,
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
