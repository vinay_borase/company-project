import 'package:flutter/material.dart';
import 'package:weekend_app/club_screen.dart';

class Home1Screen extends StatefulWidget {
  const Home1Screen({super.key});

  @override
  State<Home1Screen> createState() => _Home1ScreenState();
}

class _Home1ScreenState extends State<Home1Screen> {
  @override
  Widget build(BuildContext context) {
    String str = "c";
    String name1 = "club";

    List<Widget> topWidget(String b) {
      List<Widget> demoWidget1 = [];

      setState(() {
        str = b;

        for (int i = 1; i <= 10; ++i) {
          demoWidget1.add(
            Container(
              height: 200,
              width: 360,
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/top/${str}${i}.jpg"),
                    fit: BoxFit.fill),
                border: Border.all(color: Colors.white),
              ),
              alignment: Alignment.bottomLeft,
              margin: const EdgeInsets.all(10),
            ),
          );
        }
      });
      return demoWidget1;
    }

    List<Widget> bottomWidget(String b, String name) {
      List<Widget> demoWidget2 = [];

      setState(() {
        str = b;
        name1 = name;

        for (int i = 1; i <= 5; ++i) {
          demoWidget2.add(
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ClubScreen()));
              },
              child: Container(
                height: 170,
                width: 230,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/${name1}/${str}${i}.jpg"),
                      alignment: Alignment.topLeft,
                      fit: BoxFit.fill),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.white),
                ),
                child: Container(
                  alignment: Alignment.bottomLeft,
                  height: 30,
                  width: 230,
                  //color: Colors.blue,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15)),
                    color: Colors.white,
                  ),
                  child: const Padding(
                    padding: const EdgeInsets.all(4.5),
                    child: Row(
                      children: [
                        Text(
                          "Eskobar",
                          style: TextStyle(
                            fontSize: 17,
                          ),
                        ),
                        Spacer(),
                        Text(
                          "4.4",
                          style: TextStyle(fontSize: 17),
                        ),
                        Icon(
                          Icons.star_rate,
                          size: 19,
                        ),
                      ],
                    ),
                  ),
                ),
                alignment: Alignment.bottomLeft,
                margin: const EdgeInsets.all(10),
              ),
            ),
          );
        }
      });
      return demoWidget2;
    }

    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: AppBar(
        title: Image.asset(
          "assets/images/w1.png",
          height: 120,
          width: 150,
        ),
        backgroundColor: Colors.black,
        actions: <Widget>[
          TextButton(
            onPressed: () {},
            child: const Text(
              "login",
              style: TextStyle(
                fontSize: 17,
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: topWidget("t"),
              ),
            ),
            Container(
              child: const Text(
                "Clubs",
                style: TextStyle(
                    fontSize: 25,
                    fontStyle: FontStyle.italic,
                    color: Colors.white),
              ),
              alignment: Alignment.topLeft,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: bottomWidget("c", "club"),
              ),
            ),
            Container(
              child: const Text(
                "Trek",
                style: TextStyle(
                    fontSize: 25,
                    fontStyle: FontStyle.italic,
                    color: Colors.white),
              ),
              alignment: Alignment.topLeft,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: bottomWidget("t", "trek"),
              ),
            ),
            Container(
              child: const Text(
                "Wedding",
                style: TextStyle(
                    fontSize: 25,
                    fontStyle: FontStyle.italic,
                    color: Colors.white),
              ),
              alignment: Alignment.topLeft,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: bottomWidget("w", "wed"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
