const description1 = [
  ("  Located less than 40 kms from the city of Pune, Sinhagad is one of the most popular treks of the region. Part of the strategically important triumvirate of Sinhagad, Rajgad and Torna, it has been the centre of a number of battles."),
  ("The trail originates from the Donje Gaon Phata bus stop.The trek itself can be divided into two sections- the first stretch is a tar road from Donje Gaon that gently climbs up to the parking spot for two-wheelers. It can also be covered on a bike. The second half of the trek is a steep but incredibly rewarding climb to the top of the fort."),
];

const description2 = [
  ("Harishchandragad trek is one of the most challenging treks in the western ghats of Maharashtra. A popular trek which offers a variety of adventures to all kinds of trekkers.It is a hill fort in the Ahmednagar district situated in the Malshej Ghat. It climbs up to an altitude of 4,670 ft."),
  ("The trails from Walhivale/Belpada village are very challenging. There are three separate trails from Belpada. They are via Nalichi Vaat, Makad Naal and Sadhle ghat. You can also do the combination of Sadhle ghat and Bhailghat. Attempt these routes only if you are an experienced trekker. You need to have climbing equipment like rope and carabiner."),
];
