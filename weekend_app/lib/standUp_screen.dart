import 'package:flutter/material.dart';

class StandUpComedy extends StatefulWidget {
  const StandUpComedy({super.key});

  @override
  State<StandUpComedy> createState() => _StandUpComedyState();
}

class _StandUpComedyState extends State<StandUpComedy> {
  @override
  Widget build(BuildContext context) {
    //List<Widget> mywidgets = [];
    String str = "c";
    List<Widget> demo(String b) {
      List<Widget> mywidgets = [];
      setState(() {
        str = b;
        for (int x = 1; x <= 5; ++x) {
          mywidgets.add(
            Container(
              height: 170,
              width: 230,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/${str}${x}.jpg"),
                    alignment: Alignment.topLeft,
                    fit: BoxFit.fill),
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(color: Colors.white),
              ),
              child: Container(
                alignment: Alignment.bottomLeft,
                height: 30,
                width: 230,
                //color: Colors.blue,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15)),
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(4.5),
                  child: Row(
                    children: [
                      Text(
                        "Eskobar",
                        style: TextStyle(
                          fontSize: 17,
                        ),
                      ),
                      Spacer(),
                      Text(
                        "4.4",
                        style: TextStyle(fontSize: 17),
                      ),
                      Icon(
                        Icons.star_rate,
                        size: 19,
                      ),
                    ],
                  ),
                ),
              ),
              alignment: Alignment.bottomLeft,
            ),
          );
        }
      });
      return mywidgets;
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Standup Comdey"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              children: demo("s"),
            ),
            Row(
              children: demo("c"),
            )
          ],
        ),
      ),
    );
  }
}
