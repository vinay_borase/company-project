import 'package:flutter/material.dart';
import 'package:weekend_app/fort_info.dart';
import 'package:weekend_app/home1_screen.dart';

class BackGroundScreen extends StatefulWidget {
  const BackGroundScreen({super.key});

  @override
  State<BackGroundScreen> createState() => _BackGroundScreen();
}

class _BackGroundScreen extends State<BackGroundScreen>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(length: 3, vsync: this);
    int start1 = 0;
    int end1 = 0;
    var height1 = 0.0;
    var width1 = 0.0;
    String str = "c";
    String name1 = "club";
    String info = "text";

    String demoText(int index) {
      setState(() {
        info = description[index];
      });
      return info;
    }

    List<Widget> backScreen(
        int start, int end, var a, var c, String b, String name) {
      List<Widget> backWidget1 = [];

      setState(() {
        start1 = start;
        end1 = end;
        str = b;
        name1 = name;
        height1 = a;
        width1 = c;
        for (int i = start; i <= end; i++) {
          backWidget1.add(
          Container(
            height: height1,
            width: width1,
            child: Image.asset(
              "assets/images/${name1}/${str}${i}.jpg",
              fit: BoxFit.fill,
            ),
            color: Colors.blue,
            margin: EdgeInsets.only(left: 15, right: 10),
          ),
          );
        }
      });
      return backWidget1;
    }

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 31, 30, 30),
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Home1Screen()));
            },
            icon: const Icon(Icons.arrow_back_ios)),
        title: const Text("Club"),
        backgroundColor: Colors.black,
      ),
      body: Column(children: [
        Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    children: backScreen(2, 2, 200.0, 300.0, "s", "sinhgad"),
                  ),
                ),
                Container(
                  child: TabBar(
                      controller: _tabController,
                      labelColor: Colors.white,
                      unselectedLabelColor: Colors.white60,
                      isScrollable: true,
                      tabs: [
                        Tab(
                          text: "plan",
                        ),
                        Tab(
                          text: "Info",
                        ),
                        Tab(
                          text: "Content",
                        ),
                      ]),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          height: 400,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(15),
            child: Column(
              children: [
                Container(
                  child: Text(
                    "Description ",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  alignment: Alignment.topLeft,
                ),
                Text(
                  "${demoText(0)}",
                  style: TextStyle(fontSize: 17, color: Colors.white),
                ),
              ],
            ),
          ),
        )
      ]),
    );
  }
}
