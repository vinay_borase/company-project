import 'package:flutter/material.dart';
import 'package:weekend_app/fort_info.dart';

class SinhgadScreen extends StatefulWidget {
  const SinhgadScreen({super.key});

  @override
  State<SinhgadScreen> createState() => _SinhgadScreenState();
}

class _SinhgadScreenState extends State<SinhgadScreen>
    with TickerProviderStateMixin {
  int index = 0;
  String info = description[0];

  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(length: 3, vsync: this);

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 23, 23, 23),
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text(
          "Sinhgad Fort",
          style: TextStyle(
            fontSize: 25,
          ),
        ),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 250,
                width: 375,
                child: Image.asset(
                  "assets/images/s2.jpg",
                  fit: BoxFit.fill,
                ),
                color: Colors.blue,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),

          Container(
            height: 500,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(15),
              child: Column(
                children: [
                  Container(
                    child: TabBar(
              controller: _tabController,
              labelColor: Colors.white,
              unselectedLabelColor: Colors.white10,
              isScrollable: true,
              tabs: [
                Tab(
                  text: "Info",
                ),
                Tab(
                  text: "place",
                ),
                Tab(
                  text: "Photo",
                ),
              ],
            ),
          ),
          Container(
            height: 10,
            width: double.maxFinite,
            child: TabBarView(
              controller: _tabController,
              children: [
              Text("Hi"),
              Text("There"),
              Text("here"),
            ]),
          ),

                  Container(
                    child: Text(
                      "Description: ",
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    alignment: Alignment.topLeft,
                  ),
                  Text(
                    info,
                    style: TextStyle(fontSize: 17, color: Colors.white),
                  ),
                  Container(
                    height: 250,
                    width: 350,
                    child: Image.asset(
                      "assets/images/s3.jpg",
                      fit: BoxFit.fill,
                    ),
                    color: Colors.blue,
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    color: Colors.black,
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    color: Colors.orange,
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    color: Colors.red,
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    color: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomSheet: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Icon(Icons.currency_rupee_rounded),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.indeterminate_check_box_outlined),
            ),
            Text(
              "120",
              style:
                  TextStyle(decoration: TextDecoration.underline, fontSize: 20),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.add_box_outlined),
            ),
            Spacer(),
            ElevatedButton(
              onPressed: () {},
              child: Text(
                "Book Trip Now >>",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
